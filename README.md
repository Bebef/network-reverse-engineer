# Network Reverse Engineer

Face it, IoT security is [complete crap](https://arstechnica.com/information-technology/2020/06/upnp-flaw-exposes-millions-of-network-devices-to-attacks-over-the-internet/url).
This probably won't change, ever. Devices are not developed with a security
focus in mind, but with a "low cost" and "time to market" focus instead. Once
released, they get updated very rarely (if they get updated at all). Even *if*
they receive updates, they won't get updates forever, so if you want to continue
using those devices there inevitably will be a time when they will become a
security issue.  
  
What you probably want to do to mitigate *some* of the issues is separate IoT
devices in networks different from your "main" one and firewall them. However,
seemingly nobody of the designers think of security the least bit, making
firewalled IoT devices a nightmare to route and firewall (the eighties called,
they want their FTP protocol back!).
  
This repository contains documentation on how IoT devices communicate with
each other or with client applications (such as the corrseponding app on your
smartphone).  
The documentation is "plain", i.e. contains only network connections needed and
a few technical hints.. Practical implementation in routers/firewalls etc. is
NOT part of this documentation.  
  
Pull requests are *very* welcome of course! ;)