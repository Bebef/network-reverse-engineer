Teufel Raumfeld
===============
    
## Product/Appliance/Device
Manufacturer: Lautsprecher Teufel GmbH  
Product name(s): Teufel One S, Teufel One M, Teufel Stereo M, Teufel Sounddeck
Streaming, Cinebar Lux, Teufel Stereo L, Teufel X Rosenthal, Teufel Streamer
(aka. "Raumfeld" devices)
  
## Summary
Internet connection mandatory: not sure (needed for Spotify, TuneIn etc.)  
Uses external/not configurable NTP server: yes  
Uses questionable internet detection: not sure/not investigated yet  
  
App connects to device: yes  
Device connects to app: yes  
  
## Network connections/configuration hints
Use mDNS for discovery: yes  

### Connection to device
- TCP port 47000-58000  
Uses ports in the registered port range to connect from the app to the device.
This is the typically observed port range, but the possibility of other ports
being used can't be excluded.

### Connection from device
- TCP ports 37000-43000  
Uses ports in the registered port range to connect from the device to the app.
This is the typically observed port range, but the possibility of other ports
being used can't be excluded.

### Connection establishment hints
The Raumfeld app will discover the "host", which is configured in the app
during setup of the Raumfeld speakers, via mDNS (The host may be changed later
in the app settings). The app then connects to the "host" to gather
information about the whole setup (i.e. configuration and status of all
speakers).  
CONTROL of the speakers, such as play/pause, media selection, volume etc. seems
to be handled by the connection from the app directly to the device and/or the
"host".  
STATUS information are transmitted via a connection from the speaker to the
app. Control and playback is possible without this connection. However, for
information about what is actually playing on the speaker and the current
volume setting, the status information needs to be received by the app.
