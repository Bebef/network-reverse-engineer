Vorwerk Robot Vacuum
====================
 
## Product/Appliance/Device
Manufacturer: Vorwerk Elektrowerke GmbH & Co. KG   
Product name(s): Kobold VR 300  
  
## Summary
Internet connection mandatory: yes  
Uses external/not configurable NTP server: yes  
Uses questionable internet detection: no  
  
App connects to device: yes  
Device connects to app: no  
  
## Network connections/configuration hints
For almost all functionality, an internet connection is required. Both the
robo vaccum and the app connect to an AWS IP to exchange data and commands.
The only direct connection needed is for "Manual cleaning".  
Use mDNS for discovery: no  

### Connection to device
- TCP port 8081  

### Connection from device
There is no connection initiated from the device to the app.

### Connection establishment hints
It seems, as there is no lookup of any kind when starting "Manual cleaning",
that actual connection information is shared via the mutual cloud connection of
app and robo vacuum. If both app and robo vaccuum have a cloud connection,
"Manual cleaning" can be just started from the app if an appropriate firewall
rule/routing exists.
