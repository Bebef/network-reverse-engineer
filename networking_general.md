Networking (general information)
================================

## Questionable internet detection
There are some devices that check for an active internet connection by sending
DNS queries to the Google DNS server 8.8.8.8. What that actally does it not
checking for an active internet connection, but if a device in the network is
able to resolve DNS queries via the Google DNS server. That is not the same
thing as an active internet connection, of course.  
What if (although somewhat unlikely) the Google DNS server was down? What if
global routes were messed up? What if DNS queries to resolvers out of your
local network were blocked to
[mitigate some malware issues](https://en.wikipedia.org/wiki/DNSChanger)?  
  
It should be self explanatory that trying to query external DNS servers to
detect internet connectivity is a questionable idea. Querying a wildcard DNS
record with an arbitrary hostname for sure is the better idea. Or you probably
could just do a traceroute to see if internet connectivity. Or try to connect
to an arbitrary host on port 443...  
Fortunately, as bungling as this idea is, as bungling it is implemented most
of the time. There is no validation whatsoever. As long as a DNS query is being
replied, the check validates to "I have internet".

### Mitigation
It's quite easy to outsmart those kind of checks. Just implement a port
forwarding of any query to DNS (i.e. TCP/UDP port 53) that is outside of your
network(s) to your local DNS server. Any query to "8.8.8.8" (or another
external DNS for that matter) will happily be answered by your own DNS server
which in turn will be accepted by the checking appliance/application.

## Fixed NTP server queries
It also seems to be common practice to ignore NTP servers announced by DHCP
(option 42) and just use a hardcoded NTP server instead. Apart from ignoring
DHCP options probably is a bad idea, the NTP server might not be available
"forever", might be too many hops away or it simply might not be desired to
query arbitrary NTP servers in the Internet for security and privacy concerns.

### Mitigation
As there are no security checks in NTP, it's quite easy to create a port
forwarding rule that would redirect all traffic to NTP (TCP/UDP port 123) to
a local server instead.
